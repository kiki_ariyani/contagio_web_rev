<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once (APPPATH . 'libraries/API_Controller.php');

class Events extends API_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model('event');	
	}

/*	function add(){
		$this->check_auth_token();
		$data = json_decode(file_get_contents('php://input'), true);

		$data = array("user_id" => $data['user_id'],
					  "location_id" => $data['location_id'],
					  "name" => $data['name'],
					  "datetime" => $data['datetime'],
					  "organizer" => $data['organizer'],
					  "created_at" => $data['created_at'],
					  "updated_at" => $data['updated_at']
					  );

		if($id = $this->event->add($data)){
			$data["id"] = $id;
			$result = array(	
				'status' => 1,
				'msg' => 'Event has been successfully saved',
				'data' => $data
			);
		}else{
			$result = array(
				'status' => 0,
				'msg' => 'Something error occurred in the server. Your event cannot be saved at this time. Please try again later.'
			);
		}

		$this->response($result);
	}*/

    function save(){
        $this->load->model(array('location','event_image'));
        $this->check_auth_token();
        $data = json_decode(file_get_contents('php://input'), true);
    //    $data = array("id"=>13,"user_id"=>"2","location_id"=>"92","name"=>"Ngoding Bareng 1234","datetime"=>1447833600000,"organizer"=>"Mobilus Interactive","created_at"=>1472441123637,"updated_at"=>1472441123637,"deleted_images"=>"[11]");

        $server_id = $data['id'];
        $deleted_imgs = $data['deleted_images'];
        $deleted_imgs = json_decode($deleted_imgs);

        $data = array("user_id" => $data['user_id'],
                      "location_id" => $data['location_id'],
                      "name" => $data['name'],
                      "datetime" => $data['datetime'],
                      "organizer" => $data['organizer'],
                      "created_at" => $data['created_at'],
                      "updated_at" => $data['updated_at']
                      );

        if($server_id > 0){
            //edit
            $id = $server_id;
            $event = $this->event->get(array('id' => $id))->row_array();
            if($this->event->edit($id,$data)){
                if($event){
                    if($this->location->is_used($event['location_id'])){
                        //delete location
                        $this->location->delete($event['location_id']);
                    }
                }
            }
        }else{
            //add
            $id = $this->event->add($data);
        }

        //delete images
        foreach ($deleted_imgs as $key => $value) {
            $img = $this->event_image->get(array('id' => $value))->row_array();
            if($this->event_image->delete($value)){
                $url = "./assets/attachment/event/".$img['name'];
                if (file_exists(realpath(APPPATH . '../assets/attachment/event/') . DIRECTORY_SEPARATOR . $img['name'])) {
                    //unlink($url);
                }
            }
        }

        if($id > 0){
            $data["id"] = $id;
            $result = array(    
                'status' => 1,
                'msg' => 'Event has been successfully saved',
                'data' => $data
            );
        }else{
            $result = array(
                'status' => 0,
                'msg' => 'Something error occurred in the server. Your report cannot be saved at this time. Please try again later.'
            );
        }

        $this->response($result);
    }


	function upload_image() {
        $this->check_auth_token();
		
        if ($_FILES["file"]["error"] > 0) {
            $this->response(array(
                'status' => 0,
                'message' => $_FILES["file"]["error"],
            ));
        } else {
            $this->load->model('event_image');
            $event_id = $this->input->post('event_id');
            $name = $this->input->post('name').".jpg";
            
            $is_linked = $this->event->get(array('id' => $event_id))->row_array();

            if (!$is_linked) {
                $this->response(array(
                    'status' => 0,
                    'msg' => "Event data not found"
                ));
            }

           	$file_name = $event_id . '_' . $name;
           	$dest_file = PATH_UPLOAD_TO_EVENT_ATTACHMENT . DIRECTORY_SEPARATOR . $file_name;
            move_uploaded_file($_FILES["file"]["tmp_name"], $dest_file);

            $img = array(
                'event_id' => $event_id,
                'name' => $file_name,
                'created_at' => $this->input->post('created_at'),
                'updated_at' => $this->input->post('updated_at')
            );
            
            $img_id = $this->event_image->add($img);

            $this->response(array(
                'status' => 1,
                'event_id' => $this->input->post('event_id'),
                'id_server' => $img_id,
                'name' => $name,
                'img' => $img,
                'msg' => "Event has been successfully saved"
            ));
        }
    }

    function get_images(){
        $event_id = $this->input->post('event_id');

        $this->load->model('event_image');
        $data = $this->event_image->get(array('event_id' => $event_id))->result_array();

        if ($data) {
            $result = array(
                'status' => 1,
                'data' => $data
            );
        } else {
            $result = array(
                'status' => 0,
                'msg' => 'Data not found'
            );
        }

        $this->response($result);
    }

    function get_one(){
        $this->load->model(array('location','event_image'));
        $id = $this->input->post('id');

        if(isset($id)){
            $data = $this->event->get(array('id' => $id))->row_array();
            if($data){
                $data['location'] = $this->location->get(array('id' => $data['location_id']))->row_array();
                $data['images'] = $this->event_image->get(array('event_id' => $data['id']))->result_array();
            }
        }else{
            $data = NULL;
        }

        if (count($data) > 0) {
            $result = array(
                'status' => 1,
                'data' => $data
            );
        } else {
            $result = array(
                'status' => 0,
                'msg' => 'Data not found'
            );
        }

        $this->response($result);
    }
}