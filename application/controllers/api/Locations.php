<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once (APPPATH . 'libraries/API_Controller.php');

class Locations extends API_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model('location');	
	}

	function save(){
		$this->check_auth_token();
		$data = json_decode(file_get_contents('php://input'), true);

		$server_id = $data["id"];
		$data = array("name" => $data['name'],
					  "lat" => ($data['lat'] ? : 0),
					  "lng" => ($data['lng'] ? : 0),
					  "api_id" => ($data['api_id'] ? : ""),
					  "api_type" => ($data['api_type'] ? : 0),
					  "created_at" => $data['created_at']
					  );

		if($server_id > 0){
			//edit
			$id = $server_id;
			$this->location->edit($id, $data);
		}else{
			//add
			$location_existed = $this->location->get(array('name' => $data['name']))->row_array();

			if($location_existed){
				$this->response(array(
					'status' => 1,
					'msg' => 'Location already exist.',
					'data' => $location_existed
				));
			}

			$id = $this->location->add($data);
		}

		if($id > 0){
			$data["id"] = $id;
			$result = array(	
				'status' => 1,
				'msg' => 'Location has been successfully saved',
				'data' => $data
			);
		}else{
			$result = array(
				'status' => 0,
				'msg' => 'Something error occurred in the server. Your location cannot be saved at this time. Please try again later.'
			);
		}

		$this->response($result);
	}

	function get_list($keyword = ""){
		$this->load->model(array('report_image', 'event_image'));
		$where = "";
		
		if($keyword != ""){
			$data = $this->location->search($keyword)->result_array();
		}else{
			$lat = doubleval($this->input->post('lat'));
			$lng = doubleval($this->input->post('lng'));
			$data = $this->location->get_by_distance($lat,$lng)->result_array();	
		}
		
		foreach ($data as $key => $value) {
			if($keyword != "" && $value['type'] != -2){
				$data[$key]['latest_updates'] = $this->location->get_latest_update($value['id'], $keyword)->result_array();
			}else{
				$data[$key]['latest_updates'] = $this->location->get_latest_update($value['id'])->result_array();
			}

			foreach ($data[$key]['latest_updates'] as $i => $val) {
				if($val['report_type'] > -1){
					$images = $this->report_image->get(array('report_id' => $val['id']))->result_array();
				}else{
					$images = $this->event_image->get(array('event_id' => $val['id']))->result_array();
				}
				$data[$key]['latest_updates'][$i]['images'] = $images;
			}
			unset($data[$key]['type']);
			if($data[$key]['latest_updates'] > 0){
				$data[$key]['cover_image'] = $this->location->get_cover_image($value['id']);
			}
		}

		if (count($data) > 0) {
			$result = array(
				'status' => 1,
				'data' => $data
			);
		} else {
			$result = array(
				'status' => 0,
				'msg' => 'Data not found'
			);
		}

		$this->response($result);
	}

	function get_one(){
		$id = $this->input->post('id');

		if(isset($id)){
			$data = $this->location->get(array('id' => $id))->row_array();
		}else{
			$data = NULL;
		}

		if (count($data) > 0) {
			$result = array(
				'status' => 1,
				'data' => $data
			);
		} else {
			$result = array(
				'status' => 0,
				'msg' => 'Data not found'
			);
		}

		$this->response($result);
	}

	function get_newsfeed_item(){
		$this->load->model(array('report','event','user'));

		$type = $this->input->post('type');
		$id = $this->input->post('id');
		
		if($type == NEWSFEED_REPORT){
			$content = $this->report->get(array('id' => $id))->result_array();
		}else{
			$content = $this->event->get(array('id' => $id))->result_array();
		}

		if(count($content) > 0){
			$content[0]['report_type'] = ($type == NEWSFEED_REPORT ? $content[0]['type'] : -1);
			$content[0]['event_name'] = ($type == NEWSFEED_REPORT ? "" : $content[0]['name']);
			$user = $this->user->get(array('id' => $content[0]['user_id']))->row_array();
			$content[0]['creator'] = $user['name'];
			$data = $this->location->get(array('id' => $content[0]['location_id']))->result_array();
			if(count($data) > 0){
				$data[0]['latest_updates'] = $content;
				$data[0]['cover_image'] = $this->location->get_cover_image($data[0]['id']);
			}
		}
		
		if (count($data) > 0) {
			$result = array(
				'status' => 1,
				'data' => $data
			);
		} else {
			$result = array(
				'status' => 0,
				'msg' => 'Data not found'
			);
		}

		$this->response($result);
	}
}