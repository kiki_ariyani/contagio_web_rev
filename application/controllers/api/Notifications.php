<?php
require_once (APPPATH . 'libraries/API_Controller.php');

class Notifications extends API_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model(array('report', 'event', 'location'));	
	}

	function get(){		
		$lat = $this->input->post('lat') ?: 10;
		$lng = $this->input->post('lng') ?: 10;	

		$report = $this->report->get_notification($lat, $lng);
		$event = $this->event->get_notification($lat, $lng);

		if(count($report) > 0){
			$notif = $report;
			if(count($event) > 0){
				if($report['created_at'] < $event['created_at']){
					$notif = $event;
				}
			}
		}else{
			$notif = $event;
		}

		if(count($notif) > 0) {
			$notif['location'] = $this->location->get(array('id' => $notif['location_id']))->row_array();
			$result = array(	
				'status' => 1,
				'data' => $notif
			);
		} else {
			$result = array(
				'status' => 0,
				'msg' => "No content nearby found"
			);
		}

		$this->response($result);
	}
	
}