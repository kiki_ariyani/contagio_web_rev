<?php
require_once (APPPATH . 'libraries/API_Controller.php');

class Reports extends API_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model('report');	
	}

	function get_newsfeed_list() {
		$order_by = $this->input->post('order_by') ? : 'distance';
		$limit = $this->input->post('limit') ?: 20;
		$offset = $this->input->post('offset') ?: 0;
		$lat = $this->input->post('lat') ?: 10;
		$lng = $this->input->post('lng') ?: 10;
		$filter = $this->input->post('filter');

		$reports = $this->report->getNewsFeed($order_by, $limit, $offset, $lat, $lng, $filter);

		if(count($reports) > 0) {
			$result = array(	
				'status' => 1,
				'data' => $reports
			);
		} else {
			$result = array(
				'status' => 0,
				'msg' => "No reports found"
			);
		}

		$this->response($result);
	}

	function get_location_report(){
		$location_id = $this->input->post('location_id');

		$data = $this->report->get(array('location_id' => $location_id), 'created_at DESC')->result_array();

		if ($data) {
			$result = array(
				'status' => 1,
				'data' => $data
			);
		} else {
			$result = array(
				'status' => 0,
				'msg' => 'Data not found'
			);
		}

		$this->response($result);
	}

	function save(){
		$this->load->model(array('location','report_image'));
		$this->check_auth_token();
		$data = json_decode(file_get_contents('php://input'), true);

		$server_id = $data['id'];
		$deleted_imgs = $data['deleted_images'];
        $deleted_imgs = json_decode($deleted_imgs);

		$data = array("user_id" => $data['user_id'],
					  "location_id" => $data['location_id'],
					  "type" => $data['type'],
					  "comment" => $data['comment'],
					  "date" => $data['date'],
					  "created_at" => $data['created_at'],
					  "updated_at" => $data['updated_at'],
					  "victim" => $data['victim']
					  );

		if($server_id > 0){
			//edit
			$id = $server_id;
			$report = $this->report->get(array('id' => $id))->row_array();
			if($this->report->edit($id,$data)){
				if($report){
					if($this->location->is_used($report['location_id'])){
						//delete location
						$this->location->delete($report['location_id']);
					}
				}
			}
		}else{
			//add
			$id = $this->report->add($data);
		}

		//delete images
		foreach ($deleted_imgs as $key => $value) {
			$img = $this->report_image->get(array('id' => $value))->row_array();
           	if($this->report_image->delete($value)){
           		$url = "./assets/attachment/report/".$img['name'];
		        if (file_exists(realpath(APPPATH . '../assets/attachment/report/') . DIRECTORY_SEPARATOR . $img['name'])) {
		            unlink($url);
		        }
           	}
        }

		if($id > 0){
			$data["id"] = $id;
			$result = array(	
				'status' => 1,
				'msg' => 'Report has been successfully saved',
				'data' => $data
			);
		}else{
			$result = array(
				'status' => 0,
				'msg' => 'Something error occurred in the server. Your report cannot be saved at this time. Please try again later.'
			);
		}

		$this->response($result);
	}

	function upload_image() {
        $this->check_auth_token();
		
        if ($_FILES["file"]["error"] > 0) {
            $this->response(array(
                'status' => 0,
                'message' => $_FILES["file"]["error"],
            ));
        } else {
            $this->load->model('report_image');

            $id = $this->input->post("id");
            $report_id = $this->input->post('report_id');
            
            $is_linked = $this->report->get(array('id' => $report_id))->row_array();

            if (!$is_linked) {
                $this->response(array(
                    'status' => 0,
                    'msg' => "Report data not found"
                ));
            }
            
        	$name = $this->input->post('name').".jpg";
           	$file_name = $report_id . '_' . $name;
           	$dest_file = PATH_UPLOAD_TO_REPORT_ATTACHMENT . DIRECTORY_SEPARATOR . $file_name;
            move_uploaded_file($_FILES["file"]["tmp_name"], $dest_file);

            $img = array(
                'report_id' => $report_id,
                'name' => $file_name,
                'created_at' => $this->input->post('created_at'),
                'updated_at' => $this->input->post('updated_at')
            );
            
            $img_id = $this->report_image->add($img);

            $this->response(array(
                'status' => 1,
                'report_id' => $report_id,
                'id_server' => $img_id,
                'name' => $name,
                'img' => $img,
                'msg' => "Report has been successfully saved"
            ));
        }
    }

    function get_images(){
    	$report_id = $this->input->post('report_id');

    	$this->load->model('report_image');
    	$data = $this->report_image->get(array('report_id' => $report_id))->result_array();

    	if ($data) {
			$result = array(
				'status' => 1,
				'data' => $data
			);
		} else {
			$result = array(
				'status' => 0,
				'msg' => 'Data not found'
			);
		}

		$this->response($result);
    }

    function get_newsfeed_images(){
    	$owner_id = $this->input->post('owner_id');
    	$type = $this->input->post('type');

    	if($type == NEWSFEED_LOCATION){	
    		$this->load->model('report_image');
    		$data = $this->report_image->get_location_image($owner_id)->result_array();
    	}else if($type == NEWSFEED_REPORT){
    		$this->load->model('report_image');
    		$data = $this->report_image->get(array('report_id' => $owner_id))->result_array();
		}else{
			$this->load->model('event_image');
			$data = $this->event_image->get(array('event_id' => $owner_id))->result_array();
		}
		
		if ($data) {
			$result = array(
				'status' => 1,
				'data' => $data
			);
		} else {
			$result = array(
				'status' => 0,
				'msg' => 'Data not found'
			);
		}

		$this->response($result);
    }

    function get_one(){
    	$this->load->model(array('location','report_image'));
		$id = $this->input->post('id');

		if(isset($id)){
			$data = $this->report->get(array('id' => $id))->row_array();
			if($data){
				$data['location'] = $this->location->get(array('id' => $data['location_id']))->row_array();
				$data['images'] = $this->report_image->get(array('report_id' => $data['id']))->result_array();
			}
		}else{
			$data = NULL;
		}

		if (count($data) > 0) {
			$result = array(
				'status' => 1,
				'data' => $data
			);
		} else {
			$result = array(
				'status' => 0,
				'msg' => 'Data not found'
			);
		}

		$this->response($result);
	}

	function graph_report_by_type(){
		$case = $this->report->get(array('type' => CASES_REPORT))->num_rows();
		$breeding_grounds = $this->report->get(array('type' => BREADING_GROUNDS_REPORT))->num_rows();
		$fogging = $this->report->get(array('type' => FOGGING_REPORT))->num_rows();
		$data = array($case, $breeding_grounds, $fogging);

		if (count($data) > 0) {
			$result = array(
				'status' => 1,
				'data' => $data
			);
		} else {
			$result = array(
				'status' => 0,
				'msg' => 'Data not found'
			);
		}

		$this->response($result);
	}

	function manual_input_1611($user_id){
		$locations = array(array('name' => 'TMN DESA JAYA', 'lat' => 3.216288, 'lng' => 101.626855),
			array('name' => 'APPT RESIDENSI BISTARIA', 'lat' => 3.19647, 'lng' => 101.7665),
			array('name' => 'TMN SUNWAY BT CAVES', 'lat' => 3.237111, 'lng' => 101.690463),
			array('name' => 'AU3 TMN KERAMAT', 'lat' => 3.181043, 'lng' => 101.756821),
			array('name' => 'TMN PERMATA', 'lat' => 1.350866, 'lng' => 103.833005),
			array('name' => 'TMN UKAY BISTARI', 'lat' => 3.200508, 'lng' =>	101.765914),
			array('name' => 'TMN DAYA', 'lat' => 1.552237, 'lng' =>	103.760384),
			array('name' => 'KG SELAYANG PERMAI', 'lat' => 3.258277, 'lng' => 101.665955),
			array('name' => 'SELAYANG BARU K7', 'lat' => 3.263346, 'lng' =>	101.655193),
			array('name' => 'TMN SELAYANG UTAMA', 'lat' => 3.237264, 'lng' => 101.642682),
			array('name' => 'TMN SRI GOMBAK FASA 1', 'lat' => 3.252543, 'lng' => 101.708164),
			array('name' => 'RAWANG INTEGRATED INDUSTRIAL PARK', 'lat' => 3.307149, 'lng' => 101.561318),
			array('name' => 'TMN PERWIRA', 'lat' => 3.227596, 'lng' => 101.717504),
			array('name' => 'SRI MAYA CONDO', 'lat' => 3.175225, 'lng' => 101.733387),
			array('name' => 'TMN PRIMA SELAYANG', 'lat' => 3.250855, 'lng' => 101.644481),
			array('name' => 'TMN SRI BATU CAVES', 'lat' => 3.237884, 'lng' =>101.684039),
			array('name' => 'KG LAKSAMANA', 'lat' => 3.247726,'lng' => 101.675701),
			array('name' => 'TMN BUKIT DESA KEPONG', 'lat' => 3.2025, 'lng' => 101.637222),
			array('name' => 'APPT BKT BARU KERAMAT HUJUNG', 'lat' => 3.06378, 'lng' => 101.4679588),
			array('name' => 'APPT DESA TEMENGGONG', 'lat' => 3.253358, 'lng' => 101.691753),
			array('name' => 'TMN PINGGIRAN INDAH BT CAVES', 'lat' => 3.25126, 'lng' => 101.692547),
			array('name' => 'TMN SAMUDERA TIMUR', 'lat' => 3.233866, 'lng' => 101.698181),
			array('name' => 'TMN GOMBAK SETIA', 'lat' => 3.236966, 'lng' => 101.706783),
			array('name' => 'TMN SELAYANG MUTIARA', 'lat' => 3.258977, 'lng' =>101.665561),
			array('name' => 'KEPONG ULU', 'lat' => 3.177132, 'lng' => 101.736614),
			array('name' => 'TMN EQUINE', 'lat' => 3.003022, 'lng' => 101.679111),
			array('name' => 'BL PJU 8 APT FLORA DAMANSARA 2', 'lat' => 3.17038, 'lng' => 101.604857),
			array('name' => 'SEKSYEN 7', 'lat' => 3.074107, 'lng' => 101.492219),
			array('name' => 'SRI PJS 8 MENTARI COURT', 'lat' => 3.081337, 'lng' => 101.610711),
			array('name' => 'USJ 1 (ANGSANA APPT)', 'lat' => 3.045351, 'lng' => 101.602156),
			array('name' => 'BL PJU 9 SD 13 APT SRI MERANTI 2', 'lat' => 3.11946, 'lng' => 101.571481),
			array('name' => 'KD PJU 1A PUNCAK SRI KELANA KONDO', 'lat' => 3.111986, 'lng' => 101.58001),
			array('name' => 'BL PJU 10 APT HARMONI 2', 'lat' => 3.195743, 'lng' => 101.592908),
			array('name' => 'TMN SRI SERDANG 1', 'lat' => 5.504665, 'lng' => 100.433752),
			array('name' => 'SRI KEMBANGAN PUSAT PERDAGANGAN (JLN PSK)', 'lat' => 3.016258, 'lng' => 101.698805),
			array('name' => 'SEKSYEN 2', 'lat' => 3.06843, 'lng' => 101.51628),
			array('name' => 'SEKSYEN 7', 'lat' => 3.074107, 'lng' => 101.492219),
			array('name' => 'BL PJU 6 FLAT PELANGI DAMANSARA', 'lat' => 3.109458, 'lng' => 101.589985),
			array('name' => 'TMN SG. BESI INDAH 3', 'lat' => 3.031445, 'lng' => 101.724834),
			array('name' => 'PST BDR PUCHONG PSN INDERA', 'lat' => 3.0527, 'lng' => 101.588286),
			array('name' => 'TM PJS 2D FLAT DESA MENTARI', 'lat' => 3.076173, 'lng' => 101.633288),
			array('name' => 'BDR PUTERI 7', 'lat' => 5.611959, 'lng' => 100.532846),
			array('name' => 'KD PJU 1A PPRT B', 'lat' => 3.115378, 'lng' => 101.579973),
			array('name' => 'TMN PAIK SIONG', 'lat' => 3.061659, 'lng' => 101.64485),
			array('name' => 'SRI KEMBANGAN', 'lat' => 3.021998, 'lng' => 101.705541),
			array('name' => 'SEKSYEN 13', 'lat' => 3.08205, 'lng' => 101.538892),
			array('name' => 'SRI SS 6 KOMPLEKS KEDIAMAN KASTAM', 'lat' => 3.09821, 'lng' => 101.6018),
			array('name' => 'SS 19/4', 'lat' => 3.070463, 'lng' => 101.576291),
			array('name' => 'TMN PUCHONG PERMAI', 'lat' => 3.008913, 'lng' => 101.597644),
			array('name' => 'USJ 2/4', 'lat' => 3.060101, 'lng' => 101.589328)
			);

			$no_victim = array(72,65,51,44,42,29,28,25,22,21,21,19,19,18,18,15,14,13,11,9,9,8,6,5,5,129,85,69,42,40,33,26,26,22,21,19,18,18,17,16,14,13,13,11,11,9,8,8,7,7);

		foreach ($locations as $key => $value) {
			$this->load->model('location');
			$get_loc = $this->location->get(array('name' => $value['name']))->row_array();
			if($get_loc){
				//get location data
				echo "duplicate<br>";
				$location_id = $get_loc['id'];
			}else{
				//input location data
				echo "new<br>";
				$data_loc = array('name' => $value['name'], 'lat' => $value['lat'], 'lng' => $value['lng'],'created_at' => number_format(microtime(true)*1000,0,'.',''));
				$location_id = $this->location->add($data_loc);
			}	

			$data_report = array('user_id' => $user_id, 'location_id' => $location_id, 'type' => CASES_REPORT, 'comment' => "Manual Data Collection",'victim' => $no_victim[$key], 'date' => strtotime("2015-02-11") * 1000, 'created_at' => number_format(microtime(true)*1000,0,'.',''), 'updated_at' => number_format(microtime(true)*1000,0,'.',''));	
			$this->report->add($data_report);
		}
	}

	//this function need to be deleted before commit
	function convert_date(){
		$this->load->helper('mydate_helper');

		echo "contageo : ".to_default_date(1448422369993);
		echo "198 : ".to_default_date(1449113258567);
	}
}