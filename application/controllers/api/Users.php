<?php

require_once(APPPATH . '/libraries/API_Controller.php');

class Users extends API_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('user');
	}

	function login() {
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$last_token = $this->input->post('last_token');
		$response = $this->auth($email,$password, $last_token);	
		$this->response($response);
	}

	function register(){
		$this->load->helper("mydate_helper");
		
		$data = array("email" => ($this->input->post("email") ?  : ""),
					  "name" => ($this->input->post("name") ?  : ""),
					  "password" => ($this->input->post("password") ?  : ""),
					  "phone" => ($this->input->post("phone") ?  : ""),
					  "birthday" => ($this->input->post("birthday") ?  : ""),
					  "gender" => ($this->input->post("gender") ?  : ""),
					  "created_at" => date_in_microtime(),
					  "updated_at" => date_in_microtime());
		
		if($data["email"] != ""){
			$user_existed = $this->user->get(array('email' => $data['email']))->num_rows();

			if($user_existed > 0){
				$this->response(array(
					'status' => 0,
					'msg' => 'Email is already taken.'
				));
			}

			$new_user_id = $this->user->add($data);
			
			if ( ! $new_user_id){
				$this->response(array(
					'status' => 0,
					'msg' => 'Something error occurred in the server. Please try again later.'
				));
			}else{
				$user = $this->user->get(array('id' => $new_user_id))->row_array();
				unset($user['password']);

				//auto login
				$response = $this->auth($data['email'], $data['password']);
				$this->response($response);
			}
		}else{
			$this->response(array(
					'status' => 0,
					'msg' => 'Email is required.'
				));
		}
	}

	private function auth($email,$password, $last_token = NULL){
		if ($user = $this->user->auth($email, $password)) {
			$auth_token = $this->auth_token->generate_auth_token($user['id']);
			
			if ($last_token != NULL) {
				$this->auth_token->delete_auth_token($user['id'], $auth_token); // delete old token
			}
			
			$response = array(
				'status' => 1,
				'data' => array('user' => $user, 'auth_token' => $auth_token),
				);
		}else{								
			$error_message = "";

			$user_existed = $this->user->get(array('email' => $email))->num_rows();
			if ($user_existed == 0) {
				$error_message = "The email you entered doesn't appear to belong to an account. Please check your email and try again.";
			} else {
				$error_message = "The password you entered is incorrect. Please try again.";
			}

			$response = array(
				'status' => 0,
				'msg' => $error_message
			);	
		}

		return $response;
	}

	function login_with_facebook(){
		$this->load->helper("mydate_helper");
		
		$data = array("email" => ($this->input->post("email") ?  : ""),
					  "name" => ($this->input->post("name") ?  : ""),
					  "password" => "",
					  "gender" => ($this->input->post("gender") ?  : ""),
					  "fb_id" => ($this->input->post("fb_id") ?  : ""),
					  "created_at" => date_in_microtime(),
					  "updated_at" => date_in_microtime());
		
		if($data["email"] != ""){
			$user_existed = $this->user->get(array('email' => $data['email']))->row_array();

			if($user_existed){
				//login
				$user = $this->user->auth_with_facebook($data['email']);
				$auth_token = $this->auth_token->generate_auth_token($user['id']);

				$response = array(
					'status' => 1,
					'data' => array('user' => $user, 'auth_token' => $auth_token),
					);

				$this->response($response);
			}else{
				//register
				$data['gender'] = ($data['gender'] == "female" ? GENDER_FEMALE : GENDER_MALE);
				$new_user_id = $this->user->add($data);
			
				if ( ! $new_user_id){
					$this->response(array(
						'status' => 0,
						'msg' => 'Something error occurred in the server. Please try again later.'
					));
				}else{
					$user = $this->user->get(array('id' => $new_user_id))->row_array();
					unset($user['password']);
					$auth_token = $this->auth_token->generate_auth_token($user['id']);

					$response = array(
						'status' => 1,
						'data' => array('user' => $user, 'auth_token' => $auth_token),
					);

					$this->response($response);
				}
			}
		}else{
			$this->response(array(
					'status' => 0,
					'msg' => 'Email is required.'
				));
		}
	}

	function forget_password(){
		$this->load->helper('phpmailer');

		$email = $this->input->post('email');

		$user = $this->user->get(array('email' => $email, 'fb_id' => ""))->row_array();
		if(!$user){
			$this->response(array(
				'status' => 0,
				'msg' => 'Email is not registered as contageo user'
			));
		}

		$password = $this->user->generate_password();
		
		$subject = "Contageo";
		$message = "Hello {$user['name']}, 
                    <br/><br/>
                    Such your request, Your password has been reset. Here password is: 
                    <br/><br/>
                    Username: {$user['email']} <br/>
                    Password: {$password} 
                    <br/><br/><br/>
                    Thank you, <br/>
                    Contageo Team";

		$mail_param = array(
            'to' => $email,
            'subject' => $subject,
            'message' => $message,
        );

        $msg_email = phpmailer_send($mail_param, true);

        if($this->user->edit($user['id'], array("password" => $password))){
        	$this->response(array(
				'status' => 1,
				'msg' => 'New password has been sent to '.$email.'.'
			));
        }
	}

	function test_email(){
		$this->load->helper('phpmailer');

		$mail_param = array(
            'to' => "kiki@mobilus-interactive.com",
            'subject' => "Test Contageo",
            'message' => "Test email contageo",
            'debug' => 1
        );

        $msg_email = phpmailer_send($mail_param, true);
	}

/*	function update_pass(){
		$this->user->edit(1, array("password" => "testing"));
	}*/
}
