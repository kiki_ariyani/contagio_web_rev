<?php
class Event extends CI_Model {

	function __construct() {
		parent::__construct();		
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get('event');
	}

	function add($data){
		$this->db->insert('event', $data);
		return $this->db->insert_id();
	}

	function get_notification($lat, $lng){
		$range = 1;
		$distance = 6371;
		$current_time = round(microtime(true) * 1000);
		$const = 'constant';

		$query = "SELECT e.*, {$const('NEWSFEED_EVENT')} as content_type,
					({$distance}*acos(cos(radians({$lat})) 
						* cos( radians( l.lat ) ) 
						* cos( radians( l.lng ) - radians({$lng}) ) 
						+ sin( radians({$lat}) ) 
						* sin( radians( l.lat ) ) ) ) AS distance
				  FROM event as e
				  LEFT JOIN location as l ON l.id = e.location_id
				  WHERE e.datetime >= ({$current_time} + 7200000) /* get event at least last 2 hours */
				  HAVING distance <= {$range}
				  ORDER BY e.datetime desc";
		$result = $this->db->query($query);
		return $result->row_array();
	}

	function edit($id,$data){
		$this->db->where('id', $id);
		return $this->db->update('event', $data);
	}
}
?>