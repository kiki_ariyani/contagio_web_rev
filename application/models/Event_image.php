<?php
class Event_image extends CI_Model {

	function __construct() {
		parent::__construct();		
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get('event_image');
	}

	function add($data){
		$this->db->insert('event_image', $data);
		return $this->db->insert_id();
	}

	function delete($id){
		$this->db->where('id',$id);
		return $this->db->delete('event_image');
	}
}
?>