<?php
class Location extends CI_Model {

	function __construct() {
		parent::__construct();		
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get('location');
	}

	function get_by_distance($lat, $lng){
		$distance_from_user = 3;
		$distance = 6371;
		$query = "SELECT *, ({$distance}*acos(cos(radians({$lat})) 
						* cos( radians( lat ) ) 
						* cos( radians( lng ) - radians({$lng}) ) 
						+ sin( radians({$lat}) ) 
						* sin( radians( lat ) ) ) ) AS distance FROM location";
				  //HAVING distance <= {$distance_from_user}";

		return $this->db->query($query);
	}
	
	function add($data){
		$this->db->insert('location', $data);
		return $this->db->insert_id();
	}

	function edit($id, $data){
		$this->db->where('id', $id);
		return $this->db->update('location', $data);
	}

	function get_latest_update($location_id, $keyword = ""){
		$query = "SELECT 
					r.id, r.user_id, r.type as report_type, r.comment, r.date as date,r.victim, r.created_at, r.updated_at, '' as event_name, 0 as datetime,'' as organizer,u.name as creator
				FROM report as r
				LEFT JOIN location as l ON l.id = r.location_id
				LEFT JOIN user as u ON u.id = r.user_id
				WHERE l.id = {$location_id} ".($keyword != "" ? "AND r.comment LIKE '%".$keyword."%'" : "")."

				UNION
				SELECT 
					e.id, e.user_id, -1 as report_type, '' as comment,0 as date,0 as victim, e.created_at, e.updated_at, e.name as event_name,e.datetime, e.organizer, u.name as creator
				FROM event as e
				LEFT JOIN location as l ON l.id = e.location_id
				LEFT JOIN user as u ON u.id = e.user_id
				WHERE l.id = {$location_id} ".($keyword != "" ? "AND e.name LIKE '%".$keyword."%'" : "")."

				ORDER BY updated_at DESC";

		$result = $this->db->query($query);

		return $result;
	}

	function get_cover_image($location_id){
		$query = "SELECT 
					ri.name, r.updated_at, 0 as type
				FROM report_image as ri
				LEFT JOIN report as r ON r.id = ri.report_id
				WHERE r.location_id = {$location_id}

				UNION
				SELECT 
					ei.name, e.updated_at, -1 as type
				FROM event_image as ei
				LEFT JOIN event as e ON e.id = ei.event_id
				WHERE e.location_id = {$location_id}

				ORDER BY updated_at DESC
				LIMIT 1";

		$query = $this->db->query($query);
		$result = $query->row_array();
		$cover_image = "";
		if($result){
			$path = ($result['type'] < 0 ? "assets/attachment/event/" : "assets/attachment/report/");
			$cover_image = $path."/".$result['name'];
		}

		return $cover_image;
	}

	function search($keyword){
		$str_like = "%".$keyword."%";

		$query = "SELECT id, name, lat,lng,api_id,api_type,created_at,type FROM
				(SELECT 
					l.*, 1 as type
				FROM report as r
				LEFT JOIN location as l ON l.id = r.location_id
				WHERE r.comment LIKE '{$str_like}' 
				GROUP BY id

				UNION ALL
				SELECT 
					l.*, -1 as type
				FROM event as e
				LEFT JOIN location as l ON l.id = e.location_id
				WHERE e.name LIKE '{$str_like}'
				GROUP BY id

				UNION ALL
				SELECT 
					l.*, -2 as type
				FROM location as l
				WHERE l.name LIKE '{$str_like}') derivedTable
				GROUP BY id";

		$result = $this->db->query($query);

		return $result;
	}

	function is_used($id){
		$this->load->model(array('report','event'));
		$report = $this->report->get(array('location_id' => $id))->num_rows();
		$event = $this->event->get(array('location_id' => $id))->num_rows();

		return ($report == 0 && $event == 0);
	}

	function delete($id){
		$this->db->where('id', $id);
		return $this->db->delete('location');
	}
}
?>