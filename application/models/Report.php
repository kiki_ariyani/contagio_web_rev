<?php
class Report extends CI_Model {

	function __construct() {
		parent::__construct();		
	}

	function getNewsFeed($order_by, $limit, $offset, $lat, $lng, $filter){
		$distance = 6371; // km value
		$const = 'constant';

		$query = "SELECT 
					id, 0 as user_id, 0 as report_type, name as address, lat, lng, '' as comment, 0 as date, 0 created_at, 0 updated_at,
					0 as datetime, '' as event_name, '' as organizer,
					(select count(*) from report as r where 
						r.location_id = l.id AND r.type = {$const('CASES_REPORT')} AND r.created_at > (UNIX_TIMESTAMP() - 24 * 3600 * 7) * 1000) as case_num_week,
					IFNULL((select SUM(victim) from report as r where 
						r.location_id = l.id AND r.created_at > (UNIX_TIMESTAMP() - 24 * 3600 * 7) * 1000),0) as victims,
					(select created_at from report as r where
						r.location_id = l.id ORDER BY created_at DESC LIMIT 1) as last_case_date, '' as creator,
				    ({$distance} * acos( cos( radians({$lat}) ) 
						* cos( radians( lat ) ) 
						* cos( radians( lng ) - radians({$lng}) ) 
						+ sin( radians({$lat}) ) 
						* sin( radians( lat ) ) ) ) AS distance, 
				    {$const('NEWSFEED_LOCATION')} as type
				FROM location as l
				WHERE (select created_at from report as r where r.location_id = l.id AND r.type = {$const('CASES_REPORT')} ORDER BY created_at DESC LIMIT 1) IS NOT NULL
				AND {$const('FILTER_LOCATION')} IN ($filter)

				UNION
				SELECT 
					r.id, r.user_id, r.type as report_type, l.name as address, l.lat, l.lng, r.comment, r.date, r.created_at, r.updated_at,
					0 as datetime, '' as event_name, '' as organizer, 0 as case_num_week, 0 as victims, 0 as last_case_date, '' as creator,
				       ({$distance} * acos( cos( radians({$lat}) ) 
				              * cos( radians( l.lat ) ) 
				              * cos( radians( l.lng ) - radians({$lng}) ) 
				              + sin( radians({$lat}) ) 
				              * sin( radians( l.lat ) ) ) ) AS distance, 
				        {$const('NEWSFEED_REPORT')} as type
				FROM report as r
				LEFT JOIN location as l ON l.id = r.location_id
				WHERE r.type IN ($filter)
				/*WHERE r.type != {$const('CASES_REPORT')}*/

				UNION
				SELECT 
					e.id, e.user_id, 0 as report_type, l.name as address, l.lat, l.lng, '' as comment, 0 as date, e.created_at, e.updated_at,
					e.datetime, e.name as event_name, e.organizer, 0 as case_num_week, 0 as victims, 0 as last_case_date, u.name as creator,
				       ({$distance} * acos( cos( radians({$lat}) ) 
				              * cos( radians( l.lat ) ) 
				              * cos( radians( l.lng ) - radians({$lng}) ) 
				              + sin( radians({$lat}) ) 
				              * sin( radians( l.lat ) ) ) ) AS distance, 
				        {$const('NEWSFEED_EVENT')} as type
				FROM event as e
				LEFT JOIN location as l ON l.id = e.location_id
				LEFT JOIN user as u ON u.id = e.user_id
				WHERE {$const('FILTER_EVENT')} IN ($filter)

				HAVING last_case_date > -1
				ORDER BY {$order_by}
				LIMIT {$offset}, {$limit};";

		$result = $this->db->query($query);

		return $result->result_array();
	}

	function add($data){
		$this->db->insert('report', $data);
		return $this->db->insert_id();
	}

	function edit($id,$data){
		$this->db->where('id', $id);
		return $this->db->update('report', $data);
	}

	function get($where = NULL, $order_by = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		if($order_by != NULL){
			$this->db->order_by($order_by);
		}
		return $this->db->get('report');
	}

	function get_notification($lat, $lng){
		$range = 1;
		$distance = 6371;
		$const = 'constant';

		$query = "SELECT r.*, {$const('NEWSFEED_REPORT')} as content_type,
					({$distance}*acos(cos(radians({$lat}))
						* cos( radians( l.lat ) ) 
						* cos( radians( l.lng ) - radians({$lng}) ) 
						+ sin( radians({$lat}) ) 
						* sin( radians( l.lat ) ) ) ) AS distance
				  FROM report as r
				  LEFT JOIN location as l ON l.id = r.location_id
				  HAVING distance <= {$range}
				  ORDER BY r.created_at desc";
		$result = $this->db->query($query);
		return $result->row_array();
	}
}
?>