<?php
class Report_image extends CI_Model {

	function __construct() {
		parent::__construct();		
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get('report_image');
	}

	function get_location_image($location_id){
		$this->db->select('ri.*');
		$this->db->from('report_image as ri');
		$this->db->join('report as r', 'r.id = ri.report_id');
		$this->db->where('r.location_id', $location_id);
		return $this->db->get();
	}

	function add($data){
		$this->db->insert('report_image', $data);
		return $this->db->insert_id();
	}

	function delete($id){
		$this->db->where('id',$id);
		return $this->db->delete('report_image');
	}
}
?>